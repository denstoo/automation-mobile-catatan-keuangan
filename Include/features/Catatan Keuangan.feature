#Author: denstoo@gmail.com
Feature: Title of your feature
  I want to use this template for my feature file

  @Pengeluaran
  Scenario Outline: <scenario>
    Given I want to open application Catatan Keuangan
    And I want to tap "<jangkaWaktu>"
    When I want to tap button "Menu"
    And I want to tap "<InOut>"
    And I want to set tanggal "<tanggal>"
    And I want to set kategori "<kategori>"
    And I want to set jumlah "<jumlah>"
    And I want to set keterangan "<keterangan>"
    And I want to tap "SIMPAN"

    Examples: 
      | jangkaWaktu | InOut       | kategori | jumlah | keterangan       | tanggal     | scenario                                           |
      | Harian      | Pemasukan   | Gaji     | 400000 | Gajian           | today       | Membuat <InOut> untuk <kategori> sebanyak <jumlah> |
      | Mingguan    | Pengeluaran | Makanan  |  15000 | Beli ayam geprek | 18 Jul 2015 | Membuat <InOut> untuk <kategori> sebanyak <jumlah> |
      | Bulanan     | Pemasukan   | Kupon    |  15000 | despasito        | 18 Jul 2002 | Membuat <InOut> untuk <kategori> sebanyak <jumlah> |
