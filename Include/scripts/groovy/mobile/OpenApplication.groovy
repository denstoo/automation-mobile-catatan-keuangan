package mobile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import keyword.CatatanKuangan

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class OpenApplication {
	def common = new Common()
	@Given('I want to (.*) application Catatan Keuangan')
	def Given I_want_to_install_application_Catatan_Keuangan(String condition) {
		if (isApplicationAlready()) {
			if (condition.equalsIgnoreCase("Install")) {
				uninstallApplication()
				startApplication()
			} else {
				startExistingApplication(CatatanKuangan.appId)
			}
		} else {
			Mobile.comment("sudah membuka ga usah open lagi")
		}
		
		Mobile.tap(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : common.handling("TUTUP")]), 5, FailureHandling.OPTIONAL)
		
		for (int i=1; i<=10; i++) {
			if (Mobile.verifyElementVisible(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : common.handling("//*[@class = 'android.widget.ImageButton' and contains(@resource-id,'Menu')]")]), 1, FailureHandling.OPTIONAL) == false) {
				Mobile.tap(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : common.handling("//*[@class = 'android.widget.Button' and @resource-id = 'close-button-icon']")]), 1, FailureHandling.OPTIONAL)
				Mobile.pressBack()
			} else {
				break
			}
		}
	}
	
	def isApplicationAlready() {
		try {
			Mobile.verifyElementExist(findTestObject('Mobile/Custom/xpath', [('set') : "//*[@package = '$CatatanKuangan.appId']"]), 0)
			return false
		} catch (Exception e) {
			return true
		}
	}
	
	def uninstallApplication() {
		try {
			Mobile.startExistingApplication(CatatanKuangan.appId)
			MobileDriverFactory.getDriver().removeApp(CatatanKuangan.appId)
		} catch (Exception e) { }
	}
	
	def startApplication() {
		try {
			Mobile.startApplication(CatatanKuangan.appFile, false)
		} catch (Exception e) {
			WS.comment('PROSES INSTALL Failed')
			STOP_INSTALL_GAGAL
		}
	}
	
	def startExistingApplication(String appId) {
		for (int i=1; i<=3; i++) {
			try{
				Mobile.startExistingApplication(appId)
				break
			} catch (Exception e) {
				WS.comment('OPEN APLIKASI Failed\n' + e.toString())
				GAGAL_MEMBUKA_APLIKASI
			}
		}
	}
}