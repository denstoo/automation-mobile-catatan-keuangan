package mobile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import keyword.CatatanKuangan

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Common {
	String locator
	def tap(String locator) {
		tap(locator, "stop")
	}
	@And('I want to tap "(.*)"(.*)')
	def tap(String locator, String failureHandling) {
		Mobile.tap(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : handling(locator)]), 0, FailureHandling(failureHandling))
	}
	@And('I want to tap button "(.*)"(.*)')
	def And I_want_to_tap_button(String locator, String failureHandling) {
		locator = "//*[@class = 'android.widget.ImageButton' and contains(@resource-id,'$locator')]"
		Mobile.tap(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : handling(locator)]), 0, FailureHandling(failureHandling))
	}
	def setText(String locator,String value) {
		setText(locator, value, "stop")
	}
	def setText(String locator,String value, String failureHandling) {
		Mobile.setText(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : handling(locator)]), value, 0, FailureHandling(failureHandling))
	}
	String handling(String locator) {
		TestObject object
		if (locator.substring(0, 1) == '/') {
			locator = locator
		} else if (locator.length() >= 14 && locator.substring(0, 14) == CatatanKuangan.appId) {
			locator = "//*[@resource-id = '$locator']"
		} else if (locator.length() >= 15 && locator.substring(0, 15) == 'android.widget.') {
			locator = "//*[@class = '$locator']"
		} else {
			locator = "//*[@text = '$locator']"
		}
		WS.comment("find element $locator")
		return locator
	}
	public static FailureHandling FailureHandling(String failureHandling) {
		failureHandling = failureHandling.trim()
		WS.comment(failureHandling)
		if (failureHandling.equals("optional")) {
			return FailureHandling.STOP_ON_FAILURE
		} else if (failureHandling.equals("continue")) {
			return FailureHandling.CONTINUE_ON_FAILURE
		} else {
			return FailureHandling.STOP_ON_FAILURE
		}
	}
	
	def Swipe (int start, int finish) {
		int device_Height = Mobile.getDeviceHeight()
		int device_Width = Mobile.getDeviceWidth()
		int mid = device_Width / 2
		start = device_Height * start / 100
		finish = device_Height * finish / 100
		Mobile.comment("Scroll to Swipe dari $start ke $finish")
		Mobile.swipe(mid, start, mid, finish)
	}
}

