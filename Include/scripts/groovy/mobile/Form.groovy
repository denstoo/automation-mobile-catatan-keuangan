package mobile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Form {
	def step = new Common()
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@And('I want to set tanggal "(.*)"')
	def I_want_to_set_tanggal(String tanggal) {
		tanggal = tanggal.toLowerCase() == 'today' ? new Date().format('d MMM YYYY') : tanggal
		Mobile.comment("Memilih tanggal $tanggal")

		Mobile.tap(findTestObject('Object Repository/Mobile/Form/field tanggal'), 0)
		
		String actualYear = Mobile.getText(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : step.handling("//*[@resource-id = 'android:id/date_picker_header_year']")]), 0, FailureHandling.OPTIONAL)
		String actualDate = Mobile.getText(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : step.handling("//*[@resource-id = 'android:id/date_picker_header_date']")]), 0, FailureHandling.OPTIONAL)
		actualDate = actualDate.replaceAll(",", "")
		Mobile.comment(actualYear)
		Mobile.comment(actualDate)
		
		while (actualDate.split(" ")[2] != tanggal.split(' ')[1]) {
			Mobile.comment("tanggal berbeda $actualDate")
			Mobile.tap(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : step.handling("//*[@resource-id = 'android:id/next']")]), 0)
			step.tap(tanggal.split(' ')[0])
			actualDate = Mobile.getText(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : step.handling("//*[@resource-id = 'android:id/date_picker_header_date']")]), 0, FailureHandling.OPTIONAL)
		}
		
		
		
		Mobile.comment("expected taun " + tanggal.split(' ')[2])
		if(actualYear != tanggal.split(' ')[2]) {
			Mobile.comment("taun berbeda")
			Mobile.tap(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : step.handling("//*[@resource-id = 'android:id/date_picker_header_year']")]), 0)
			step.Swipe(40, 90)
			step.Swipe(40, 90)
			for (int i=1; i<=20; i++) {
				if (Mobile.verifyElementVisible(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : step.handling(tanggal.split(' ')[2])]), 1, FailureHandling.OPTIONAL) == false) {
					step.Swipe(60, 45)
				} else {break}
			}
			Mobile.scrollToText(tanggal.split(' ')[2])
			Mobile.comment("expected taun " + tanggal.split(' ')[2])
			Mobile.tap(findTestObject('Object Repository/Mobile/Custom/xpath', ['set' : step.handling(tanggal.split(' ')[2])]), 0)
		}
		
		
		
		step.tap('OK')
	}
	
	@And('I want to set kategori "(.*)"')
	def I_want_to_set_kategori(String kategori) {
		Mobile.tap(findTestObject('Object Repository/Mobile/Form/field kategori'), 0)
		step.tap(kategori)
	}
	@And('I want to set jumlah "(.*)"')
	def I_want_to_set_jumlah(String jumlah) {
		Mobile.setText(findTestObject('Object Repository/Mobile/Form/field jumlah'), jumlah, 0)
	}
	@And('I want to set keterangan "(.*)"')
	def I_want_to_set_keterangan(String keterangan) {
		Mobile.setText(findTestObject('Object Repository/Mobile/Form/field keterangan'), keterangan, 0)
	}

	@When("I check for the (\\d+) in step")
	def I_check_for_the_value_in_step(int value) {
		println value
	}

	@Then("I verify the (.*) in step")
	def I_verify_the_status_in_step(String status) {
		println status
	}
}